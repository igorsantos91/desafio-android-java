//package br.com.concrete.desafio;
//
//import android.content.Intent;
//import android.support.test.rule.ActivityTestRule;
//import android.support.test.runner.AndroidJUnit4;
//
//import net.vidageek.mirror.dsl.Mirror;
//
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//
//import br.com.concrete.desafio.retrofit.RetrofitConfig;
//import okhttp3.OkHttpClient;
//import okhttp3.logging.HttpLoggingInterceptor;
//import okhttp3.mockwebserver.MockResponse;
//import okhttp3.mockwebserver.MockWebServer;
//import retrofit2.Retrofit;
//import retrofit2.converter.gson.GsonConverterFactory;
//
//import static android.support.test.espresso.Espresso.onView;
//import static android.support.test.espresso.assertion.ViewAssertions.matches;
//import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
//import static android.support.test.espresso.matcher.ViewMatchers.withId;
//
//@RunWith(AndroidJUnit4.class)
//public class MainActivityTest {
//
//    public static final String OK_REPOSITORIES_PG1_JSON = "200_repositories_pg1.json";
//    private MockWebServer server;
//
//    @Rule
//    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class, false, false);
//
//    @Test
//    public void whenActivityIsLaunched_shouldDisplayInitialState() throws IOException {
//        server.url("/");
//        server.enqueue(new MockResponse()
//                .setResponseCode(200)
//                .addHeader("Content-Type", "application/json; charset=utf-8")
//                .setBody(readFile(OK_REPOSITORIES_PG1_JSON)));
//
//        mActivityRule.launchActivity(new Intent());
//
//        onView(withId(R.id.repositoriesList)).check(matches(isDisplayed()));
//    }
//
//    @Before
//    public void setUp() throws Exception {
//        server = new MockWebServer();
//        server.start();
//        String url = server.url("/").toString();
//
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//        Retrofit mockRetrofit = new Retrofit.Builder()
//                .baseUrl(url)
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build();
//
//        setField(RetrofitConfig.getRetrofit(), "retrofit", mockRetrofit);
//    }
//
//    private String readFile(String file) throws IOException {
//        StringBuilder sb = new StringBuilder();
//        InputStream in = this.getClass().getClassLoader().getResourceAsStream(file);
//        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
//        String line;
//        while((line = br.readLine()) != null){
//            sb.append(line);
//        }
//
//        br.close();
//        return sb.toString();
//    }
//
//    @After
//    public void tearDown() throws IOException {
//        server.shutdown();
//    }
//
//    private void setField(Object target, String fieldName, Object value) {
//        new Mirror()
//                .on(target)
//                .set()
//                .field(fieldName)
//                .withValue(value);
//    }
//}
