package br.com.concrete.desafio.service;

import org.junit.Test;

import br.com.concrete.desafio.entity.Github;
import br.com.concrete.desafio.entity.Repository;
import br.com.concrete.desafio.retrofit.RetrofitConfig;

import static junit.framework.Assert.*;

/**
 * Created by Igor Fernandes on 02/03/2018.
 */
public class GithubServiceTest {

    @Test
    public void buscaRepositorios() throws Exception {
        RetrofitConfig retrofitConfig = new RetrofitConfig();
        GithubService githubService = retrofitConfig.getRetrofit().create(GithubService.class);

        Github github = githubService.searchRepositories(1).execute().body();

        assertNotNull(github);
        assertFalse(github.getRepositories().isEmpty());

        for (Repository repo: github.getRepositories()) {
            System.out.println(repo.getName());
        }
    }

}