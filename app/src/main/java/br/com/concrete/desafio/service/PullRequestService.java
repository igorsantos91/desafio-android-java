package br.com.concrete.desafio.service;

import java.util.List;

import br.com.concrete.desafio.entity.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by igorf on 13/03/2018.
 */

public interface PullRequestService {

    @GET("/repos/{criador}/{repositorio}/pulls")
    Call<List<PullRequest>> getPullRequests(@Path("criador") String criador, @Path("repositorio") String repositorio);
}
