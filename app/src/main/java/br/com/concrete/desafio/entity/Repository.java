package br.com.concrete.desafio.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by igorf on 04/03/2018.
 */

public class Repository implements Serializable{

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("full_name")
    private String fullName;

    @SerializedName("description")
    private String description;

    @SerializedName("owner")
    private Owner owner;

    @SerializedName("stargazers_count")
    private int stars;

    @SerializedName("forks")
    private int forks;

    @SerializedName("pulls_url")
    private String pullRequests;

    public Repository(long id, String name, String fullName, String description, Owner owner, int stars, int forks, String pullRequests) {
        this.id = id;
        this.name = name;
        this.fullName = fullName;
        this.description = description;
        this.owner = owner;
        this.stars = stars;
        this.forks = forks;
        this.pullRequests = pullRequests;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getDescription() {
        return description;
    }

    public Owner getOwner() {
        return owner;
    }

    public int getStars() {
        return stars;
    }

    public int getForks() {
        return forks;
    }

    public String getPullRequests() {
        return pullRequests;
    }
}
