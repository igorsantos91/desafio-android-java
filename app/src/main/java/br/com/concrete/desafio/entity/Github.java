package br.com.concrete.desafio.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by igorf on 05/03/2018.
 */

public class Github {

    private int page;

    @SerializedName("items")
    private List<Repository> repositories;

    public Github(List<Repository> repositories, int page) {
        this.repositories = repositories;
        this.page = page;
    }

    public List<Repository> getRepositories() {
        if( repositories == null ) repositories = new ArrayList<>();

        return repositories;
    }

    public int getPage() {
        return page;
    }
}
