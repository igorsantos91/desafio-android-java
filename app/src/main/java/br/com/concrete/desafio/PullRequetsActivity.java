package br.com.concrete.desafio;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.concrete.desafio.adapter.PullRequestAdapter;
import br.com.concrete.desafio.entity.PullRequest;
import br.com.concrete.desafio.entity.Repository;
import br.com.concrete.desafio.listener.ItemClickListener;
import br.com.concrete.desafio.retrofit.RetrofitConfig;
import br.com.concrete.desafio.service.PullRequestService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PullRequetsActivity extends AppCompatActivity implements ItemClickListener {

    private TextView pullRequestsTextView;
    private TextView emptyPullRequestsTextView;
    private ArrayList<PullRequest> pullRequestsList;
    private PullRequestAdapter mAdapter;
    private ProgressBar mProgressBar;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requets);

        Repository repository = (Repository) getIntent().getSerializableExtra("repository");
        this.setTitle(repository.getFullName());

        pullRequestsTextView = findViewById(R.id.pullRequestsTextView);
        pullRequestsTextView.setText(null);
        emptyPullRequestsTextView = findViewById(R.id.emptyPullRequestsTextView);
        mProgressBar = findViewById(R.id.progressBarPullRequests);
        RecyclerView pullRequestsRecyclerView = findViewById(R.id.pullRequestsList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        pullRequestsRecyclerView.setLayoutManager(mLayoutManager);
        pullRequestsList = new ArrayList<>();

        if( savedInstanceState != null){
            mProgressBar.setVisibility(View.GONE);
            pullRequestsList = (ArrayList<PullRequest>) savedInstanceState.getSerializable("pullRequestsList");
            updateTexts();
        }else {
            loadPullRequests(repository);
        }

        mAdapter = new PullRequestAdapter(this, pullRequestsList, this);
        pullRequestsRecyclerView.setAdapter(mAdapter);
    }

    private void loadPullRequests(Repository repository) {
        PullRequestService pullRequestService = RetrofitConfig.getRetrofit().create(PullRequestService.class);
        pullRequestService.getPullRequests(repository.getOwner().getLogin(), repository.getName()).enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                mProgressBar.setVisibility(View.GONE);
                if(!response.isSuccessful()) {
                    pullRequestsTextView.setText(response.errorBody() != null? response.errorBody().toString(): "Erro ao processar requisição");
                }

                pullRequestsList.addAll(response.body());
                if(!pullRequestsList.isEmpty()) {
                    mAdapter.notifyDataSetChanged();
                }
                updateTexts();
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                if(t instanceof IOException){
                    Toast.makeText(PullRequetsActivity.this, "Erro de conexão", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(PullRequetsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(int position) {
        PullRequest pullRequest = pullRequestsList.get(position);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(pullRequest.getUrl()));
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        state.putSerializable("pullRequestsList", pullRequestsList);
    }

    private void updateTexts() {
        int color = Color.GRAY;
        if(pullRequestsList.isEmpty()) {
            emptyPullRequestsTextView.setVisibility(View.VISIBLE);
            color = Color.RED;
        }

        String text = String.format("%d openeds", pullRequestsList.size());
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(text);

        ssBuilder.setSpan(new ForegroundColorSpan(color),
                0,
                text.indexOf(" "),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        pullRequestsTextView.setText(ssBuilder);// / x closed
    }
}
