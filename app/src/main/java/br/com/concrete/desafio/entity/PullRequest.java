package br.com.concrete.desafio.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by igorf on 13/03/2018.
 */

public class PullRequest implements Serializable {

    @SerializedName("id")
    private long id;

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String body;

    @SerializedName("user")
    private Owner owner;

    @SerializedName("html_url")
    private String url;

    public PullRequest(long id, String title, String body, Owner owner, String url) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.owner = owner;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getUrl() {
        return url;
    }
}
