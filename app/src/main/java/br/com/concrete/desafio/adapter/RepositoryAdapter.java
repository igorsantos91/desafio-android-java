package br.com.concrete.desafio.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.concrete.desafio.R;
import br.com.concrete.desafio.entity.Repository;
import br.com.concrete.desafio.listener.ItemClickListener;

/**
 * Created by Igor Fernandes on 06/03/2018.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder>{

    private final Context context;
    private List<Repository> repositories;
    private ItemClickListener mClickListener;

    public RepositoryAdapter(Context context, List<Repository> repositories, ItemClickListener mClickListener) {
        this.context = context;
        this.repositories = repositories;
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public RepositoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_repository, parent, false);

        return new ViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Repository repository = repositories.get(position);

        holder.repositoryNameTextView.setText(repository.getName());
        holder.descriptionTextView.setText(repository.getDescription());
        holder.ownerTextView.setText(repository.getOwner().getLogin());
        holder.ownerNameTextView.setVisibility(View.GONE);//Pegar nome do autor completo
        holder.forksTextView.setText(String.valueOf(repository.getForks()));
        holder.starsTextView.setText(String.valueOf(repository.getStars()));
        Glide.with(context)
                .load(repository.getOwner().getAvatarUrl())
                .into(holder.imgRepository);

        if(repository.getOwner().getAvatarUrl() != null)
            holder.imgRepository.setBackground(null);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView repositoryNameTextView;
        public TextView descriptionTextView;
        public TextView ownerTextView;
        public TextView ownerNameTextView;
        public TextView forksTextView;
        public TextView starsTextView;
        ImageView imgRepository;
        private ItemClickListener mClickListener;

        ViewHolder(View view, ItemClickListener mClickListener){
            super(view);
            repositoryNameTextView = view.findViewById(R.id.repositoryNameTextView);
            descriptionTextView = view.findViewById(R.id.descriptionTextView);
            ownerTextView = view.findViewById(R.id.ownerTextView);
            ownerNameTextView = view.findViewById(R.id.ownerNameTextView);
            forksTextView = view.findViewById(R.id.forksTextView);
            starsTextView = view.findViewById(R.id.starsTextView);
            imgRepository = view.findViewById(R.id.itemRepositoryImageview);
            this.mClickListener = mClickListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onClick(getAdapterPosition());
        }
    }
}
