package br.com.concrete.desafio.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.concrete.desafio.R;
import br.com.concrete.desafio.entity.PullRequest;
import br.com.concrete.desafio.listener.ItemClickListener;

/**
 * Created by igorf on 14/03/2018.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder>{

    private final Context context;
    private List<PullRequest> pullRequests;
    private ItemClickListener mClickListener;

    public PullRequestAdapter(Context context, List<PullRequest> pullRequests, ItemClickListener mClickListener) {
        this.context = context;
        this.pullRequests = pullRequests;
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_pull_requests, parent, false);

        return new ViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);

        holder.titlePullRequestTextView.setText(pullRequest.getTitle());
        holder.descriptionPullRequestTextView.setText(pullRequest.getBody());
        holder.usernamePullRequestTextView.setText(pullRequest.getOwner().getLogin());
        holder.fullNamePullRequestTextView.setVisibility(View.GONE);

        Glide.with(context)
                .load(pullRequest.getOwner().getAvatarUrl())
                .into(holder.imgPullRequest);

        if(pullRequest.getOwner().getAvatarUrl() != null)
            holder.imgPullRequest.setBackground(null);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private ItemClickListener mClickListener;
        public TextView titlePullRequestTextView;
        public TextView descriptionPullRequestTextView;
        public TextView usernamePullRequestTextView;
        public TextView fullNamePullRequestTextView;
        public ImageView imgPullRequest;

        ViewHolder(View view, ItemClickListener mClickListener){
            super(view);
            this.mClickListener = mClickListener;
            titlePullRequestTextView = view.findViewById(R.id.titlePullRequestTextView);
            descriptionPullRequestTextView = view.findViewById(R.id.descriptionPullRequestTextView);
            usernamePullRequestTextView = view.findViewById(R.id.usernamePullRequestTextView);
            fullNamePullRequestTextView = view.findViewById(R.id.fullNamePullRequestTextView);
            imgPullRequest = view.findViewById(R.id.imgPullRequest);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onClick(getAdapterPosition());
        }
    }
}
