package br.com.concrete.desafio.listener;

/**
 * Created by Igor Fernandes on 12/03/2018.
 */

public interface ItemClickListener {
    void onClick(int position);
}
