package br.com.concrete.desafio.service;

import br.com.concrete.desafio.entity.Github;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Igor Fernandes on 02/03/2018.
 */

public interface GithubService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<Github> searchRepositories(@Query("page") int page);

}
