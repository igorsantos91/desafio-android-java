package br.com.concrete.desafio.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by igorf on 04/03/2018.
 */

public class Owner implements Serializable{

    @SerializedName("id")
    private int id;

    @SerializedName("login")
    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public Owner(int id, String login, String avatarUrl) {
        this.id = id;
        this.login = login;
        this.avatarUrl = avatarUrl;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
