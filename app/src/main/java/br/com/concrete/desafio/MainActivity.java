package br.com.concrete.desafio;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import br.com.concrete.desafio.adapter.RepositoryAdapter;
import br.com.concrete.desafio.entity.Github;
import br.com.concrete.desafio.entity.Repository;
import br.com.concrete.desafio.listener.ItemClickListener;
import br.com.concrete.desafio.retrofit.RetrofitConfig;
import br.com.concrete.desafio.service.GithubService;
import br.com.concrete.desafio.util.EndlessRecyclerViewScrollListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements ItemClickListener {

    private RepositoryAdapter mAdapter;
    private SwipeRefreshLayout swipeContainer;
    private ArrayList<Repository> repositories;
    private int currentPage;
    private ProgressBar mProgressBar;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView repositoriesList = findViewById(R.id.repositoriesList);
        swipeContainer = findViewById(R.id.swipeContainer);
        mProgressBar = findViewById(R.id.progressBarRepositories);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        repositoriesList.setLayoutManager(mLayoutManager);

        if( savedInstanceState != null){
            currentPage = savedInstanceState.getInt("currentPage");
            repositories = (ArrayList<Repository>) savedInstanceState.getSerializable("repositories");
            stopLoaders();
        }else {
            currentPage = 1;
            repositories = new ArrayList<>();
            updateRepositories(currentPage);
        }

        mAdapter = new RepositoryAdapter(this, repositories, this);
        repositoriesList.setAdapter(mAdapter);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                repositories.clear();
                mAdapter.notifyDataSetChanged();
                updateRepositories(1);
            }
        });

        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                updateRepositories(currentPage + 1);
            }
        };
        repositoriesList.addOnScrollListener(scrollListener);
    }

    private void updateRepositories(int page){
        currentPage = page;
        GithubService githubService = RetrofitConfig.getRetrofit().create(GithubService.class);
        githubService.searchRepositories(currentPage).enqueue(new Callback<Github>() {
            @Override
            public void onResponse(Call<Github> call, Response<Github> response) {
                Github github = response.body();
                repositories.addAll(github.getRepositories());
                mAdapter.notifyDataSetChanged();
                stopLoaders();
            }
            @Override
            public void onFailure(Call<Github> call, Throwable t) {
                stopLoaders();
                if(t instanceof IOException){
                    Toast.makeText(MainActivity.this, "Erro de conexão", Toast.LENGTH_SHORT).show();
                    System.out.println("\n\nERROR:"+t.getMessage());
                }else{
                    Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    System.out.println("\n\nERROR:"+t.getMessage());
                }
            }
        });
    }

    private void stopLoaders() {
        if(swipeContainer.isRefreshing())
            swipeContainer.setRefreshing(false);
        if(mProgressBar.getVisibility() == View.VISIBLE)
            mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(int position) {
        Repository repository = repositories.get(position);

        Intent intent = new Intent(MainActivity.this, PullRequetsActivity.class);
        intent.putExtra("repository", repository);
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        state.putSerializable("repositories", repositories);
        state.putInt("currentPage", currentPage);
    }
}
