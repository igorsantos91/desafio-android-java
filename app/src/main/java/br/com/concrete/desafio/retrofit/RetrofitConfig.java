package br.com.concrete.desafio.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Igor Fernandes on 02/03/2018.
 */

public class RetrofitConfig {

    private static Retrofit retrofit = null;

    public RetrofitConfig(){
    }

    public static Retrofit getRetrofit() {
        if(retrofit == null)
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit;
    }
}
